import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../auth/auth.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
 export class HeaderComponent implements OnInit {

  isLoggedIn = true ;
  username = "";

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit(): void {
    this.authService.loggedIn.subscribe((data: boolean) => this.isLoggedIn = data);
    this.authService.username.subscribe((data: string) => this.username = data);
    this.isLoggedIn = this.authService.isLoggedIn();
    this.username = this.authService.getUserName();
  }

  goToUserProfile() {
    this.router.navigateByUrl('/user-profile/' + this.username).then(r => {

    });
  }

  logout() {
    this.authService.logout();
    this.isLoggedIn = false;
    this.router.navigate([''], {queryParams: {logout: 'true'}}).then(r =>{
      console.info('navigate successful');
    });
  }

}
