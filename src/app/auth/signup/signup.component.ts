import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {SignupRequestPayload} from "./singup-request.payload";
import {AuthService} from "../auth.service";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html'
})
export class SignupComponent implements OnInit {

  signupRequestPayload: SignupRequestPayload;
  signupForm: FormGroup;

  constructor(private authService: AuthService, private router: Router,
              private toastR: ToastrService, fb: FormBuilder) {
    this.signupRequestPayload = {
      lastname: '',
      firstname: '',
      username: '',
      password: ''
    };

    this.signupForm = fb.group(this.signupRequestPayload);
  }

  ngOnInit(): void {
    this.signupForm = new FormGroup({
      lastname: new FormControl('', Validators.required),
      firstname: new FormControl('', Validators.required),
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  signup(): void {
    this.signupRequestPayload.lastname = this.signupForm.get('lastname')?.value;
    this.signupRequestPayload.firstname = this.signupForm.get('firstname')?.value;
    this.signupRequestPayload.username = this.signupForm.get('username')?.value;
    this.signupRequestPayload.password = this.signupForm.get('password')?.value;

    this.authService.signup(this.signupRequestPayload)
      .subscribe(data => {
        this.router.navigate(['/login'],
          {queryParams: {registered: 'true'}}).then(r =>{
            console.info('navigate successful');
        });
       }, error => {
        this.toastR.error('Registration Failed! Please try again');
      });
  }
}
