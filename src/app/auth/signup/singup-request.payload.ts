export interface SignupRequestPayload {
    username: string;
    password: string;
    lastname: string;
    firstname: string;
}
