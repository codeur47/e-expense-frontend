import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {LoginRequestPayload} from "./login-request.payload";
import {ActivatedRoute, Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {throwError} from "rxjs";
import {AuthService} from "../auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loginRequestPayload: LoginRequestPayload;

  constructor(private authService: AuthService, private activatedRoute: ActivatedRoute,
              private router: Router, private toastR: ToastrService, fb: FormBuilder) {
    this.loginRequestPayload = {
      username: '',
      password: ''
    };

    this.loginForm = fb.group(this.loginRequestPayload);
  }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });

    this.activatedRoute.queryParams
      .subscribe(params => {
        if (params.registered !== undefined && params.registered === 'true') {
          this.toastR.success('Signup Successful ! 😀');
        }
        if (params.logout !== undefined && params.logout === 'true') {
          this.toastR.success('Logout Successful ! 😀');
        }
      });
  }

  login(): void {
    this.loginRequestPayload.username = this.loginForm.get('username')?.value;
    this.loginRequestPayload.password = this.loginForm.get('password')?.value;

    this.authService.login(this.loginRequestPayload).subscribe(data => {
      this.router.navigate(['/dashboard'], {queryParams: {login: 'true'}}).then(r =>
        console.info('Successful login')
      );
    }, error => {
      throwError(error);
      this.toastR.error('Login Failed. Please check your credentials and try again.  😟');
    });
  }


}
