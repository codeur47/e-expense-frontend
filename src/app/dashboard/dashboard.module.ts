import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from "./dashboard.component";
import {HeaderComponent} from "../header/header.component";
import {FooterComponent} from "../footer/footer.component";
import {DialogModule} from "./dialog/dialog.module";

@NgModule({
  declarations: [HeaderComponent, DashboardComponent, FooterComponent],
  imports: [CommonModule, RouterModule, DialogModule],
  exports: [HeaderComponent, DashboardComponent, FooterComponent]
})
export class DashboardModule{}
