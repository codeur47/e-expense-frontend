import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {DialogType} from "./shared/dialog.type";
import {MatDialog} from "@angular/material/dialog";
import {DialogComponent} from "./dialog/dialog.component";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {CategoryRequestPayload} from "./shared/category.request.payload";
import {PurchaseRequestPayload} from "./shared/purchase.request.payload";
import {AuthService} from "../auth/auth.service";
import {DashboardService} from "./shared/dashboard.service";
import {CategoryResponsePayload} from "./shared/category.response.payload";
import {PurchaseResponsePayload} from "./shared/purchase.response.payload";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit{

  delete_purchase = DialogType.DELETE_PURCHASE;
  create_purchase = DialogType.CREATE_PURCHASE;
  update_purchase = DialogType.UPDATE_PURCHASE;

  create_category = DialogType.CREATE_CATEGORY;
  delete_category = DialogType.DELETE_CATEGORY;
  update_category = DialogType.UPDATE_CATEGORY;

  dialogType = "";
  username = "";

  categoryForm: FormGroup;
  purchaseForm: FormGroup;
  categoryRequestPayload: CategoryRequestPayload = {
    name: '',
    username: ''
  }
  purchaseRequestPayload : PurchaseRequestPayload = {
    name: '',
    description: '',
    price: 0,
    quantity: 0,
    username: '',
    categoryId: ''
  }

  categories : CategoryResponsePayload[] = [];
  purchases: PurchaseResponsePayload[] = [];

  elementId:number = 0;

  constructor(private activatedRoute: ActivatedRoute,private toastR: ToastrService,
              private dialog: MatDialog, fb: FormBuilder, private authService: AuthService,
              private dashboardService: DashboardService) {
    this.categoryForm = fb.group(this.categoryRequestPayload);
    this.purchaseForm = fb.group(this.purchaseRequestPayload);
  }

  ngOnInit() {
    this.activatedRoute.queryParams
      .subscribe(params => {
        if (params.login !== undefined && params.login === 'true') {
          this.toastR.success('Welcome back  😀');
        }
      });

    this.username = this.authService.getUserName();

    this.initCategoryForm();
    this.initPurchaseForm();
    this.getAllCategories();
    this.getAllPurchases();
  }

  openDialog(dialogType: string, elementId?: number) {
    this.dialogType = dialogType;
    if (elementId != null)
      this.elementId = elementId;
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        categoryForm: this.categoryForm,
        purchaseForm: this.purchaseForm,
        dialogType: this.dialogType,
        categories: this.categories
      },
      minWidth: '500px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (this.dialogType === this.create_category){
          this.createCategory();
        }
        if(this.dialogType == this.create_purchase){
          this.createPurchase();
        }
        if (this.dialogType == this.delete_purchase){
          this.deletePurchase(this.elementId);
        }
        if (this.dialogType == this.delete_category){
          this.deleteCategory(this.elementId);
        }
      }
    })
  }

  initCategoryForm(): void {
    this.categoryForm = new FormGroup({
      name: new FormControl('', Validators.required)
    });
  }

  initPurchaseForm(): void {
    this.purchaseForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      price: new FormControl(0, Validators.required),
      quantity: new FormControl(0, Validators.required),
      categoryId: new FormControl('', Validators.required),
    });
  }

  createCategory(): void {
    this.categoryRequestPayload.name = this.categoryForm.get('name')?.value;
    this.categoryRequestPayload.username = this.username;

    this.dashboardService.saveCategory(this.categoryRequestPayload).subscribe(
      result => {
        console.info(JSON.stringify(result));
        this.toastR.success('Category saved successfully 😀');
      }, error => {
        this.toastR.error('Category saved failed 😟');
      })
  }

  createPurchase(): void {
    this.purchaseRequestPayload.name = this.purchaseForm.get('name')?.value;
    this.purchaseRequestPayload.description = this.purchaseForm.get('description')?.value;
    this.purchaseRequestPayload.price = this.purchaseForm.get('price')?.value;
    this.purchaseRequestPayload.quantity = this.purchaseForm.get('quantity')?.value;
    this.purchaseRequestPayload.categoryId = this.purchaseForm.get('categoryId')?.value;
    this.purchaseRequestPayload.username = this.username;

    this.dashboardService.savePurchase(this.purchaseRequestPayload).subscribe(
      result => {
        this.toastR.success('Purchase saved successfully 😀');
      }, error => {
        this.toastR.error('Purchase saved failed 😟');
      })
  }

  getAllCategories(): void {
    this.dashboardService.getAllCategories().subscribe(
      result => {
        this.categories = result;
      }, error => {
        this.toastR.error('Failed to load categories 😟');
      });
  }

  getAllPurchases(): void {
    this.dashboardService.getAllPurchases().subscribe(
      result => {
      this.purchases = result;
      }, error => {
        this.toastR.error('Failed to load purchases 😟');
      })
  }

  deletePurchase(purchaseId: number): void {
    this.dashboardService.deletePurchase(purchaseId).subscribe(
      result => {
        this.toastR.success('Purchase deleted successfully 😀');
      }, error =>  {
        this.toastR.error('Failed to delete purchase 😟');
      });
  }

  deleteCategory(purchaseId: number): void {
    this.dashboardService.deleteCategory(purchaseId).subscribe(
      result => {
        this.toastR.success('Category deleted successfully 😀');
      }, error => {
        this.toastR.error('Failed to delete category 😟');
      }
    )
  }


}
