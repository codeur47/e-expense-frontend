import {Component, Inject, OnInit} from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import {DialogType} from "../shared/dialog.type";

@Component({
  templateUrl: './dialog.component.html'
})
export class DialogComponent implements OnInit {

  delete_purchase = DialogType.DELETE_PURCHASE;
  create_purchase = DialogType.CREATE_PURCHASE;
  update_purchase = DialogType.UPDATE_PURCHASE;

  create_category = DialogType.CREATE_CATEGORY;
  delete_category = DialogType.DELETE_CATEGORY;
  update_category = DialogType.UPDATE_CATEGORY;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    console.log(data);
  }

  ngOnInit() {
  }
}
