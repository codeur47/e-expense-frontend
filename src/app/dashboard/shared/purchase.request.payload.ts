export interface PurchaseRequestPayload {
  name: string;
  description: string;
  quantity: number;
  price: number;
  username:string
  categoryId:string
}
