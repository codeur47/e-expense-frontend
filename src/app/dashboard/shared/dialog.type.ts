export enum DialogType {
  DELETE_CATEGORY = 'DELETE_CATEGORY',
  DELETE_PURCHASE = 'DELETE_PURCHASE',

  CREATE_CATEGORY = 'CREATE_CATEGORY',
  CREATE_PURCHASE = 'CREATE_PURCHASE',

  UPDATE_PURCHASE = 'UPDATE_PURCHASE',
  UPDATE_CATEGORY = 'UPDATE_CATEGORY'
}
