export interface CategoryRequestPayload {
  name: string;
  username: string;
}
