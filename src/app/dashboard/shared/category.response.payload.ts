export interface CategoryResponsePayload {
  categoryId: number;
  name: string;
  createdDate: string;
}
