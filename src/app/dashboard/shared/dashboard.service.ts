import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {CategoryRequestPayload} from "./category.request.payload";
import {PurchaseRequestPayload} from "./purchase.request.payload";
import {CategoryResponsePayload} from "./category.response.payload";
import {PurchaseResponsePayload} from "./purchase.response.payload";

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private httpClient: HttpClient){}

  saveCategory(category: CategoryRequestPayload): Observable<any>{
    return this.httpClient.post('http://localhost:8080/api/category', category, { responseType: 'text' });
  }

  savePurchase(purchase: PurchaseRequestPayload): Observable<any>{
    return this.httpClient.post('http://localhost:8080/api/purchase', purchase, { responseType: 'text' });
  }

  getAllCategories(): Observable<CategoryResponsePayload[]> {
    return this.httpClient.get<CategoryResponsePayload[]>('http://localhost:8080/api/category');
  }

  getAllPurchases(): Observable<PurchaseResponsePayload[]> {
    return this.httpClient.get<PurchaseResponsePayload[]>('http://localhost:8080/api/purchase');
  }

  deletePurchase(purchaseId: number): Observable<any>{
    return this.httpClient.delete('http://localhost:8080/api/purchase/'+purchaseId, { responseType: 'text' })
  }

  deleteCategory(categoryId: number): Observable<any>{
    return this.httpClient.delete('http://localhost:8080/api/category/'+categoryId, { responseType: 'text' })
  }
}
