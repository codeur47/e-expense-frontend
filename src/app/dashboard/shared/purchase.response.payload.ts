import {CategoryResponsePayload} from "./category.response.payload";

export interface PurchaseResponsePayload {
  purchaseId: number
  name: string;
  description: string;
  quantity: number;
  price: number;
  total: number;
  category: CategoryResponsePayload;
}
